[settings]
    arch=x86_64
    build_type=Release
    compiler=Visual Studio
    compiler.runtime=MD
    compiler.version=14
    os=Windows

[requires]
    zlib/1.Y.Z

[options]
    386=False
    no_asm=False
    no_bf=False
    no_cast=False
    no_des=False
    no_dh=False
    no_dsa=False
    no_fpic=False
    no_hmac=False
    no_md2=False
    no_md5=False
    no_mdc2=False
    no_rc2=False
    no_rc4=False
    no_rc5=False
    no_rsa=False
    no_sha=False
    no_sse2=False
    no_threads=False
    no_zlib=False
    shared=True

[full_settings]
    arch=x86_64
    build_type=Release
    compiler=Visual Studio
    compiler.runtime=MD
    compiler.version=14
    os=Windows

[full_requires]
    zlib/1.2.11@conan/stable:63da998e3642b50bee33f4449826b2d623661505

[full_options]
    386=False
    no_asm=False
    no_bf=False
    no_cast=False
    no_des=False
    no_dh=False
    no_dsa=False
    no_fpic=False
    no_hmac=False
    no_md2=False
    no_md5=False
    no_mdc2=False
    no_rc2=False
    no_rc4=False
    no_rc5=False
    no_rsa=False
    no_sha=False
    no_sse2=False
    no_threads=False
    no_zlib=False
    shared=True
    zlib:minizip=False
    zlib:shared=False

[recipe_hash]
    8acc13c05d6f0bd47350d23f73bc2d8e

[env]

